package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.Key;
import java.time.Duration;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");
        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));

        String[] usernames = {"narendramodi", "barackobama", "rahulgandhi"};
        String weblink = "https://foller.me";

        File f = new File("src/test/resources/db.json");
        StringBuffer usersData = new StringBuffer("{\"users\":[");
        for (int i=0; i<usernames.length; i++) {
                String user = usernames[i];
                StringBuffer userData = extractData(driver, weblink, wait, f, user);
                usersData.append(userData);
                if(i != (usernames.length-1)) {
                    usersData.append(",");
                }
        }
        usersData.append("]}");
        write(usersData, f);
        driver.quit();
    }

    private static StringBuffer extractData(WebDriver driver, String weblink, WebDriverWait wait, File f, String user) {
        driver.get(weblink);
        StringBuffer data = new StringBuffer();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='header-search-input']")));

        driver.findElement(By.xpath("//input[@id='header-search-input']")).sendKeys(user, Keys.ENTER);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='content-top px1 py075']")));

        String username = driver.findElement(By.xpath("//li/p[@class='gray']")).getText();
        String name = driver.findElement(By.xpath("//h1[1]")).getText();
        String image = driver.findElement(By.xpath("//div[@class='col md-col-1 col-2 mt075']/img")).getAttribute("src");
        String updated_date = driver.findElement(By.xpath("//span[@class='breadcrumb xsmall gray small']")).getText();
        String isVerified = driver.findElement(By.xpath("//h1[1]/span")).getText();
        String bio = driver.findElement(By.xpath("//p[@class='mt075 mb075']")).getText();
        String location = driver.findElement(By.xpath("//div[@class='md-col md-col-6 col-12']//div[1]/span")).getText();
        String language_preference = driver.findElement(By.xpath("//div[@class='md-col md-col-6 col-12']//div[2]/span")).getText();
        String joined = driver.findElement(By.xpath("//div[@class='md-col md-col-6 col-12']//div[3]/span")).getText();
        String url = driver.findElement(By.xpath("//div[@class='md-col md-col-6 col-12']//div[4]/span")).getText();
        String followers = driver.findElement(By.xpath("//div[@class='md-col md-col-6 col-12 md-pl3']//div[1]/span")).getText();
        String following = driver.findElement(By.xpath("//div[@class='md-col md-col-6 col-12 md-pl3']//div[2]/span")).getText();
        String ratio = driver.findElement(By.xpath("//div[@class='md-col md-col-6 col-12 md-pl3']//div[3]/span")).getText();
        String tweets = driver.findElement(By.xpath("//div[@class='md-col md-col-6 col-12 md-pl3']//div[4]/span")).getText();
        String listed = driver.findElement(By.xpath("//div[@class='md-col md-col-6 col-12 md-pl3']//div[5]/span")).getText();

        String mentions = driver.findElement(By.xpath("//div[@class='content-main lg-px1 lg-py075']/div[@class='max-width-3 mx-auto pb1 clearfix']/div[@class='md-col md-col-8 col-12']/div[@class='card card-body']/div[1]/div[2]")).getText().split(" ")[0];
        String hashtags = driver.findElement(By.xpath("//div[@class='content-main lg-px1 lg-py075']/div[@class='max-width-3 mx-auto pb1 clearfix']/div[@class='md-col md-col-8 col-12']/div[@class='card card-body']/div[2]/div[2]")).getText().split(" ")[0];
        String links = driver.findElement(By.xpath("//div[@class='content-main lg-px1 lg-py075']/div[@class='max-width-3 mx-auto pb1 clearfix']/div[@class='md-col md-col-8 col-12']/div[@class='card card-body']/div[3]/div[2]")).getText().split(" ")[0];
        String media = driver.findElement(By.xpath("//div[@class='content-main lg-px1 lg-py075']/div[@class='max-width-3 mx-auto pb1 clearfix']/div[@class='md-col md-col-8 col-12']/div[@class='card card-body']/div[4]/div[2]")).getText().split(" ")[0];
        String retweets = driver.findElement(By.xpath("//div[@class='content-main lg-px1 lg-py075']/div[@class='max-width-3 mx-auto pb1 clearfix']/div[@class='md-col md-col-8 col-12']/div[@class='card card-body']/div[5]/div[2]")).getText().split(" ")[0];
        String replies = driver.findElement(By.xpath("//div[@class='content-main lg-px1 lg-py075']/div[@class='max-width-3 mx-auto pb1 clearfix']/div[@class='md-col md-col-8 col-12']/div[@class='card card-body']/div[6]/div[2]")).getText().split(" ")[0];
        //Add most linked domains
        StringBuffer tweets_per_hour = new StringBuffer("[");
        StringBuffer tweets_in_px = new StringBuffer("[");
        for(int i=0; i<24; i++) {
            String tweet = driver.findElement(By.xpath("//div[@class='hours']//div["+ (i+1)+"]/a")).getAttribute("title").split(" ")[0];
            String tweet_in_px = driver.findElement(By.xpath("//div[@class='hours']//div["+ (i+1)+"]/a")).getCssValue("height");
            tweets_per_hour.append("\"" + tweet + "\"");
            tweets_in_px.append("\"" + tweet_in_px + "\"");
            if(i != 23) {
                tweets_per_hour.append(",");
                tweets_in_px.append(",");
            }
        }

        tweets_per_hour.append("]");
        tweets_in_px.append("]");


        List<WebElement> topics = driver.findElements(By.xpath("//body/div[@class='content-main lg-px1 lg-py075']/div[@class='max-width-3 mx-auto pb1 clearfix']/div[@class='md-col md-col-8 col-12']/div[2]/p/a"));
        StringBuffer topicsArray = stringifyTopics(topics);
        List<WebElement> hashtags_used = driver.findElements(By.xpath("//body/div[@class='content-main lg-px1 lg-py075']/div[@class='max-width-3 mx-auto pb1 clearfix']/div[@class='md-col md-col-8 col-12']/div[3]/p/a"));
        StringBuffer hashtagsArray = stringifyHashtags(hashtags_used);
        List<WebElement> mentioned_accounts = driver.findElements(By.xpath("//body/div[@class='content-main lg-px1 lg-py075']/div[@class='max-width-3 mx-auto pb1 clearfix']/div[@class='md-col md-col-8 col-12']/div[4]/p/a"));
        StringBuffer mentionedArray = stringifyMentions(mentioned_accounts);
        data.append("{" +
                "\"username\":\"" + username + "\"," +
                "\"name\":\"" + name + "\"," +
                "\"image\":\"" + image + "\"," +
                "\"isVerified\":\"" + isVerified + "\"," +
                "\"updated_date\":\"" + updated_date + "\"," +
                "\"bio\":\"" + bio + "\"," +
                "\"location\":\"" + location + "\"," +
                "\"language_preference\":\"" + language_preference + "\"," +
                "\"joined\":\"" + joined + "\"," +
                "\"url\":\"" + url + "\"," +
                "\"followers\":\"" + followers + "\"," +
                "\"following\":\"" + following + "\"," +
                "\"ratio\":\"" + ratio + "\"," +
                "\"tweets\":\"" + tweets + "\"," +
                "\"listed\":\"" + listed + "\"," +
                "\"tweet_analysis\":{" +
                    "\"mentions\":\"" + mentions + "\"," +
                    "\"hashtags\":\"" + hashtags  + "\"," +
                    "\"links\":\"" + links + "\"," +
                    "\"media\":\"" + media + "\"," +
                    "\"retweets\":\"" + retweets + "\"," +
                    "\"replies\":\"" + replies + "\"," +
                    "\"activity_time\":{" +
                    "\"tweets_per_hour\":" + tweets_per_hour + "," + //Array
                    "\"tweets_in_px\":" + tweets_in_px +   //Array
                    "}" +
                "}," +
                "\"topics\":" + topicsArray + "," +   //Array
                "\"hashtags_used\":" + hashtagsArray + "," + //Array
                "\"mentioned_accounts\":" + mentionedArray +  //Array of objects
            "}");
        return data;
//        write(data, f);
    }

    private static void write(StringBuffer s, File f) {
        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream(f, false), true);
            pw.println(s);
            pw.flush();
            pw.close();
        } catch(IOException e) {
            System.out.println("Caught : " + e);
        }
    }

    private static StringBuffer stringifyTopics(List<WebElement> list) {
        StringBuffer topics = new StringBuffer("[");
        int i = 0;
        for(WebElement a: list) {
            topics.append("{\"id\":\""+ (++i) + "\",\"name\":\"" + a.getText() + "\",\"font-size\":\"" + a.getCssValue("font-size") + "\"}");
            if(i != list.size()) {
                topics.append(",");
            }
        }
        topics.append("]");
        return topics;
    }

    private static StringBuffer stringifyHashtags(List<WebElement> list) {
        StringBuffer hashtags = new StringBuffer("[");
        for(int i=0; i<list.size(); i++) {
            hashtags.append("\"" + list.get(i).getText() + "\"");
            if(i != list.size()-1) {
                hashtags.append(",");
            }
        }
        hashtags.append("]");
        return hashtags;
    }

    private static StringBuffer stringifyMentions(List<WebElement> list) {
        StringBuffer mentioned_accounts = new StringBuffer("[");
        int i=0;
        for(WebElement a: list) {
            String href = a.getAttribute("href");
            WebElement imgTag = a.findElement(By.cssSelector("img"));
            String imgSrc = imgTag.getAttribute("src");
            String imgTitle = imgTag.getAttribute("title");
            String[] imgInfo = imgTitle.split(" ");

            mentioned_accounts.append("{" +
                    "\"id\":\"" + (++i) + "\"," +
                    "\"url\":\"" + href + "\"," +
                    "\"img\":\"" + imgSrc + "\"," +
                    "\"username\":\"" + imgInfo[0] + "\"," +
                    "\"number_of_mentions\":\"" + imgInfo[3] + "\"" +
                 "}"
            );

            if(i != list.size()) {
                mentioned_accounts.append(",");
            }
        }
        mentioned_accounts.append("]");
//        System.out.println(mentioned_accounts);
        return mentioned_accounts;
    }
}
